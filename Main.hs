{-# LANGUAGE OverloadedStrings #-}
module Main where

import CLIO
import Data.ByteString.Char8 as B
import Control.Exception

ioPutStrLn s = ioPutStr (s ++ "\n")
ioPrint x = ioPutStrLn (show x)

tainted :: DCLabel -> DCLabel
tainted l = l { dcAvailability = cTrue }

alice = principal "Alice" %% True
charlie = principal "Charlie" %% True
bob = principal "Bob" %% True

aliceAndBob = principal "Alice" /\ principal "Bob" %% True
aliceOrBob = principal "Alice" \/ principal "Bob" %% True

secretValue = B.replicate 5 '3'

defaultClio l km = clio l km dcPublic (DCLabel cFalse cTrue cTrue)

test = do km <- initializeKeyMapIO [principal "Alice", principal "Charlie"]
          defaultClio alice km $ do
            x <- label alice (3 :: Integer)
            y <- label (charlie) (42 :: Integer)
            z <- label (tainted charlie) (43 :: Integer)
            toLabeled alice $ do
              a <- unlabel x
              if a == 3
                then store "foo" y
                else return ()
            m <- fetch "foo" z
            r <- unlabel m
            if r == 42
              then error "Charlie knows a == 3"
              else error "Charlie knows a /= 3"
            return ()

aliceCodeA :: DC ()
aliceCodeA = do
  lv <- label aliceOrBob secretValue
  lv' <- label alice secretValue
  store "number" lv
  store "foo" lv'
  return ()

aliceCodeB ::DC ()
aliceCodeB = do defaultVal <- label (tainted alice) (B.pack "42")
                ioPutStrLn "Fetching  key \"number\"..."
                ln <- fetch "number" defaultVal
                n <- unlabel ln
                ioPutStrLn "Fetching  key \"foo\"..."
                lm <- fetch "foo" defaultVal
                m <- unlabel lm
                ioPutStr "Checking if the retrieved values match the original ones: "
                ioPrint (n == secretValue && m == secretValue)
                return ()
          
main :: IO ()
main =  (do
  B.putStr " * Generating Alice and Bob's key pairs and populating keymap..."
  akm <- initializeKeyMapIO [principal "Alice", principal "Bob"]
  let km = adjustKey (principal "Bob") SigningKey Nothing akm -- remove's Bob's ability to sign
  B.putStrLn "Done."
  B.putStrLn " * Running Alice's code..."
  defaultClio alice km aliceCodeA
  B.putStrLn " * Alice's storing phase finished. Feel free to play with the DB now. Press any key to run the rest of Alice's code (fetching phase)."
  getChar
  defaultClio alice km aliceCodeB) `finally` (B.putStrLn " * Cleaning up redis db..." >> ioDBCleanup)
  
