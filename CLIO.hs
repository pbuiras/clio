module CLIO
  ( label, unlabel, labelOf
  , store, fetch
  , toLabeled
  , initializeKeyMapIO
  , getLabel, getClearance, lowerClearance
  , clio, DC, evalCLIO, dcDefaultState, clioPC, clioClr, clioKeyMap, clioStoreLabel
  , ioPutStr, ioDBCleanup
  , Serializable(..)
  , module CLIO.KeyMap
  , module CLIO.DC)
 where

import CLIO.Core
import CLIO.Store
import CLIO.DC
import CLIO.KeyMap
import CLIO.Crypto (initializeKeyMapIO)
import Control.Monad.Trans
import Database.Redis
import Control.Exception

clio :: DCLabel -> KeyMap -> DCLabel -> DCLabel -> DC a -> IO a
clio storeLabel km pc clr m =
  do evalCLIO (dcDefaultState { clioPC = pc, clioClr = clr
                           , clioKeyMap = km
                           , clioStoreLabel = storeLabel })
              m
       `onException` ioDBCleanup
     
ioDBCleanup :: IO ()
ioDBCleanup = do conn <- checkedConnect defaultConnectInfo
                 runRedis conn flushdb
                 return ()

ioPutStr s = ioTCB (putStr s)
