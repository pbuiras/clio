{-# LANGUAGE ViewPatterns, FlexibleInstances, ScopedTypeVariables #-}
module CLIO.Crypto where

import Control.Monad

import CLIO.DC
import CLIO.Core
import CLIO.Util (chunksOf)
import CLIO.KeyMap

import Crypto.PubKey.RSA
import Crypto.PubKey.RSA.OAEP
import Crypto.PubKey.RSA.PSS
import Crypto.Hash.Algorithms
import qualified Data.Map as M
import Control.Monad.IO.Class
import Control.Exception
import Control.Monad
import Data.Maybe (catMaybes,fromJust,isJust)

import qualified Data.ByteString.Char8 as B
import qualified Data.Map as M
import qualified Data.Set as S

import Crypto.Cipher.AES (AES256)
import Crypto.Cipher.Types
import Crypto.MAC.HMAC
import Crypto.Error (CryptoFailable(..))
import Crypto.Random

import Debug.Trace

-- | AES key size in bytes
aesKeySize = 32
-- | AES IV size
aesIVSize = 16

cipherInitNoErr :: (Cipher c, BlockCipher c) => Key c -> c
cipherInitNoErr (Key k) = case cipherInit k of
      CryptoPassed a -> a
      CryptoFailed e -> error (show e)

data Key cipher = Key B.ByteString
type SymKey = Key AES256
           
cipherMakeKey :: Cipher cipher => cipher -> B.ByteString -> SymKey 
cipherMakeKey _ = Key

-- | RSA key size in bytes
rsaKeySize = 128

-- | Exponent required to generate RSA key pairs. Using a value
-- recommended by cryptonite docs.
magicExponent = 0x10001


class Encrypt pk where
  clioEncrypt :: pk -> B.ByteString -> DC B.ByteString
  clioSignAndEncrypt :: pk -> B.ByteString -> DC B.ByteString
class Decrypt sk where
  clioDecrypt :: sk -> B.ByteString -> DC B.ByteString
  clioDecryptAndVerify :: sk -> B.ByteString -> DC B.ByteString
  
defaultOAEPParamsSHA256 :: OAEPParams SHA256 B.ByteString B.ByteString
defaultOAEPParamsSHA256 = defaultOAEPParams SHA256

defaultPSSParamsSHA256 :: PSSParams SHA256 B.ByteString B.ByteString
defaultPSSParamsSHA256 = defaultPSSParams SHA256

instance (BlockCipher cipher, Cipher cipher) => Encrypt (Key cipher) where
  clioEncrypt k m = do r <- ioTCB $ getRandomBytes aesIVSize
                       let Just msgIV = makeIV (r :: B.ByteString)
                       let ctx = cipherInitNoErr k
                           enc = ctrCombine ctx msgIV m
                       --ioTCB $ print (B.length m)
                       return (r `B.append` enc)
  clioSignAndEncrypt k m = undefined
instance (BlockCipher cipher, Cipher cipher) => Decrypt (Key cipher) where
  clioDecrypt k m = do let (r,actualm) = B.splitAt aesIVSize m
                           Just msgIV = makeIV (r :: B.ByteString)
                           ctx = cipherInitNoErr k
                           dec = ctrCombine ctx msgIV actualm
                       --ioTCB $ print (B.length m)
                       return dec
  clioDecryptAndVerify k m = undefined

{- onion crypto -}
instance Encrypt a => Encrypt [a] where
  clioEncrypt pks v = chainCrypto clioEncrypt v pks
  clioSignAndEncrypt pks v = undefined
instance Decrypt a => Decrypt [a] where
  clioDecrypt sks b = chainCrypto clioDecrypt b (reverse sks)
  clioDecryptAndVerify sks b = undefined

chainCrypto f b ks = foldM (\ct k -> f k ct) b ks

clioSign sk v = ioTCB $ signSafer defaultPSSParamsSHA256 sk v
clioVerify pk m sig = verify defaultPSSParamsSHA256 pk m sig

{- multiple signatures -}
clioMultiSign sks v = mapM (\sk -> do Right s <- clioSign sk v; return s) sks
clioMultiVerify pks v sigs = all (\(pk,sig) -> clioVerify pk v sig) (zip pks sigs)

{- | Generates two RSA key pairs, one for encryption and another for
   signatures.
-}
--generateKeyPairs :: DC KeyPairs
generateKeyPairs =
  do enckp <- liftIO $ generate rsaKeySize magicExponent
     sigkp <- liftIO $ generate rsaKeySize magicExponent
     return (mkFullKeyPairs enckp sigkp)

{- | Fetches the RSA public/private key pair for the given principal in the
  CLIO key map. 
  Sanity check: never call before checking the store
-}
fetchKeyPairPrincipal :: Principal -> DC KeyPairs
fetchKeyPairPrincipal p =
  CLIO $ do km <- getKeyMap
            case M.lookup p km of
              Just keypair -> return keypair
              Nothing -> fail $ "No KeyPairs for principal " ++ show p

rsaEncryptArbSize :: PublicKey -> B.ByteString -> DC B.ByteString
rsaEncryptArbSize pk m =
  do let chunkedm = chunksOf (rsaKeySize `div` 3) m
     encms <-
       mapM (\b -> do ee <- ioTCB $ encrypt defaultOAEPParamsSHA256 pk b
                      case ee of
                        Left err -> fail (show err)
                        Right e -> return e) chunkedm
     return (serialize encms)

rsaDecryptArbSize :: PrivateKey -> B.ByteString -> DC B.ByteString
rsaDecryptArbSize sk m =
   case deserialize m of
     Right bs ->
       do ps <- mapM (\b -> ioTCB $ decryptSafer defaultOAEPParamsSHA256 sk b) bs
          if any isLeft ps
            then {- ioTCB (putStrLn "ouch") >> -} fail "decryption error"
            else return $ B.concat [r | Right r <- ps]
  where isLeft (Left _) = True
        isLeft _ = False

rsaSignThenEncryptArbSize :: PublicKey -> PrivateKey -> B.ByteString -> DC B.ByteString
rsaSignThenEncryptArbSize epk ssk m =
  do Right sig <- clioSign ssk m
     let m' = serialize (m, sig)
     rsaEncryptArbSize epk m'

rsaDecryptThenVerifyArbSize :: PublicKey -> PrivateKey -> B.ByteString -> DC B.ByteString
rsaDecryptThenVerifyArbSize spk esk m =
  do m' <- rsaDecryptArbSize esk m
     case deserialize m' of
       Right (m'', sig) | clioVerify spk m'' sig ->
                            return m''
       _ -> fail "Deserialization or verification error in rsaDecryptThenVerifyArbSize"

instance Serializable (Key AES256) where
  serialize (Key k) = serialize k
  deserialize b = case deserialize b of
    Left e -> Left e
    Right k -> Right (cipherMakeKey (undefined :: AES256) k)

fetchKeyPairsCategory :: [Principal] -> DC [KeyPairs]
fetchKeyPairsCategory [] = return []
fetchKeyPairsCategory (p:ps) =
  do xs <- ((:[]) <$> fetchKeyPairPrincipal p) `catchCLIO`
       (\(e :: SomeException) -> return [])
     ps' <- fetchKeyPairsCategory ps
     return (xs ++ ps')


allPairs :: [a] -> [b] -> [(a,b)]
allPairs xs ys = [ (x,y) | x <- xs, y <- ys ]

fetchCK :: Disjunction -> DC (Maybe (SymKey,KeyPairs))
fetchCK d =
  do r <- clioRedisGet (B.pack $ show d)
     case r of
       Right (Just m) ->
         do --ioTCB $ print m
            let Right (publics, (ct, sig)) = deserialize m
                Right (epk,spk) = deserialize publics
            kps <- fetchKeyPairsCategory (S.toAscList $ dToSet d)
            let spks = concatMap (\kp ->
                        case hasVerificationKey kp of
                          Just k -> [k]
                          Nothing -> []) kps
            if any (\k -> clioVerify k (serialize (publics, ct)) sig) spks
              then do let esks = concatMap (\kp ->
                                        case hasDecryptionKey kp of
                                          Just k -> [k]
                                          Nothing -> []) kps
                          Right cts = deserialize ct
                      ps <- mapM (\(esk,b) ->
                                    (Just <$> rsaDecryptArbSize esk b) `catchCLIO` (\(e::SomeException) -> return Nothing)) (allPairs esks cts) -- hacky!
                      --ioTCB $ print ps
                      let xs = catMaybes ps
                      when (null xs) $ do
                        km <- CLIO getKeyMap
                        fail $ "category key decryption failure: no valid decryption keys available for " ++ show d ++ " in keymap for principals " ++ show (M.keys km)
                      let secrets = head xs
                      let Right (symkey, (esk, ssk)) = deserialize secrets
                      return (Just (symkey, mkFullKeyPairs (epk,esk) (spk,ssk)))
              else do
                km <- CLIO getKeyMap
                fail $ "category key verification failure: no valid verification keys available for " ++ show d ++ " in keymap for principals " ++ show (M.keys km)
       _ -> return Nothing
       where isRight (Right _) = True
             isRight _ = False

isSingletonPrincipal d = S.size (dToSet d) == 1

generateKeysD :: Disjunction -> DC (SymKey, KeyPairs)
generateKeysD d =
  do kps <- if (isSingletonPrincipal d)
            then fetchKeyPairPrincipal (S.findMin (dToSet d))
            else CLIO generateKeyPairs
     symkey <- ioTCB $ getRandomBytes aesKeySize
     return (cipherMakeKey (undefined :: AES256) symkey, kps)

fetchKeysD :: Disjunction -> DC (SymKey, KeyPairs)
fetchKeysD d =
  do mck <- fetchCK d
     case mck of
       Just (symkey, kps) -> {- update local keymap if needed -} return (symkey, kps)
       Nothing -> -- assumes: there is at least one signing key in the keymap
         do (symkey, kps@(allKeys -> (epk,esk,spk,ssk))) <- generateKeysD d
            let secrets = serialize (symkey, (esk, ssk))
                publics = serialize (epk,spk)
            let ps =  S.toAscList (dToSet d)
            cts <- forM ps $ \p ->
              -- encrypt the secrets with PKs of each of the
              -- principals in the category
              do (hasEncryptionKey -> Just principalPK) <- fetchKeyPairPrincipal p 
                 rsaEncryptArbSize principalPK secrets
            let ct = serialize cts
            let pubm = serialize (publics, ct)
            sig <- foldM (\s p ->
                            do (hasSigningKey -> principalSK) <-
                                 fetchKeyPairPrincipal p
                               case principalSK of
                                 Nothing  -> {- not a signing principal -} return s
                                 Just ssk -> do Right newsig <- ioTCB (putStrLn ("Signed by " ++ show p)) >> clioSign ssk pubm
                                                return newsig) errMsg ps
                                  -- write everything to the store
            let m = serialize (publics, (ct, sig))
            ioTCB $ putStrLn $ "Storing entry for category " ++ (show d)
            clioRedisSet (B.pack $ show d) m
            return (symkey, kps)
       where errMsg = error $ "no principal in keymap able to sign category " ++ show d

fetchVerifKeysD :: Disjunction -> DC KeyPair
fetchVerifKeysD d | isSingletonPrincipal d = signKP <$> fetchKeyPairPrincipal (S.findMin (dToSet d))
fetchVerifKeysD d =
  do r <- clioRedisGet (B.pack $ show d)
     case r of
       Right (Just m) ->
         do --ioTCB $ print m
            let Right (publics, (ct, sig)) = deserialize m :: Either String (B.ByteString, (B.ByteString, B.ByteString))
                Right (epk,spk) = deserialize publics :: Either String (PublicKey, PublicKey)
            kps <- fetchKeyPairsCategory (S.toAscList $ dToSet d)
            let spks = concatMap (\kp ->
                        case hasVerificationKey kp of
                          Just k -> [k]
                          Nothing -> []) kps
            if any (\k -> clioVerify k (serialize (publics, ct)) sig) spks
              then return (KeyPair (Just spk) Nothing)
              else do
                km <- CLIO getKeyMap
                fail $ "category key verification failure: no valid verification keys available for " ++ show d ++ " in keymap for principals " ++ show (M.keys km)
       _ -> fail $ "Unable to fetch public keys for non-existing category " ++ show d
       where isRight (Right _) = True
             isRight _ = False

                         
fetchSymKeysFor :: DCLabel -> DC [SymKey]
fetchSymKeysFor l = map fst <$> fetchKeyPairsFor (dcSecrecy l)

fetchKeyPairsFor :: CNF -> DC [(SymKey,KeyPairs)]
fetchKeyPairsFor cnf =
     mapM fetchKeysD (S.toList $ cToSet cnf)

fetchVerifKeyPairsFor :: CNF -> DC [KeyPair]
fetchVerifKeyPairsFor cnf =
     mapM fetchVerifKeysD (S.toList $ cToSet cnf)

fetchEncryptionKeysFor :: DCLabel -> DC [PublicKey]
fetchEncryptionKeysFor l = map (publicKey . encKP . snd) <$> fetchKeyPairsFor (dcSecrecy l)

fetchDecryptionKeysFor :: DCLabel -> DC [PrivateKey]
fetchDecryptionKeysFor l = map (privateKey . encKP . snd) <$> fetchKeyPairsFor (dcSecrecy l)

fetchSignatureKeysFor  :: DCLabel -> DC [PrivateKey]
fetchSignatureKeysFor l = map (privateKey . signKP . snd) <$> fetchKeyPairsFor (dcIntegrity l)

fetchVerificationKeysFor  :: DCLabel -> DC [PublicKey]
fetchVerificationKeysFor l =
  map publicKey <$> fetchVerifKeyPairsFor (dcIntegrity l)

initializeKeyMap :: [Principal] -> DC KeyMap
initializeKeyMap ps =
  do mapM_ fetchKeyPairPrincipal ps
     CLIO $ getKeyMap

initializeKeyMapIO :: [Principal] -> IO KeyMap
initializeKeyMapIO ps =
  do kps <- mapM (\p -> do
                        ekp <- generate rsaKeySize magicExponent
                        skp <- generate rsaKeySize magicExponent
                        return (p, mkFullKeyPairs ekp skp)) ps
     return (foldr (uncurry M.insert) M.empty kps)
