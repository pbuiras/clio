{-# LANGUAGE DeriveDataTypeable, DeriveFunctor, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances, FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses, OverloadedStrings #-}
module CLIO.Core where

import  Control.Monad
import  Data.Bits
import  qualified Data.ByteString.Char8 as S
import Data.Hashable
import  Data.List
import  Data.Monoid ()
import  Data.Set (Set)
import  qualified Data.Set as Set
import  Data.String
import  Data.Typeable
import  Data.Word
import  Text.Read
import qualified Data.ByteString.Base64 as B64

import Crypto.PubKey.RSA
import Crypto.Random

import Control.Exception

import CLIO.DC
import CLIO.Label
import qualified Control.Monad.State.Strict as ST
import Control.Monad.IO.Class
import qualified Control.Monad.Trans as T
import qualified Database.Redis as DB
import CLIO.KeyMap

import qualified Data.Map as M

type CounterMap = M.Map S.ByteString Integer

data CLIOState l = CLIOState { clioPC :: !l, clioClr :: !l,
                               clioKeyMap :: !KeyMap, clioStoreLabel :: !l,
                               clioReplayCounter :: !CounterMap,
                               clioRedisConn :: DB.Connection }

newtype CLIO l a = CLIO { unCLIO :: ST.StateT (CLIOState l) DB.Redis a }
  deriving (Functor, Applicative, Monad)

replayCounterSize :: Int
replayCounterSize = 16

runCLIO :: Label l => CLIOState l -> CLIO l a -> IO (a, CLIOState l)
runCLIO st m = do conn <- DB.checkedConnect DB.defaultConnectInfo
                  DB.runRedis conn $ ST.runStateT (unCLIO m) (st { clioRedisConn = conn })

evalCLIO :: Label l => CLIOState l -> CLIO l a -> IO a
evalCLIO st m = fmap fst $ runCLIO st m

throwCLIO :: Exception e => e -> CLIO l a
throwCLIO = ioTCB . throwIO

catchCLIO :: (Label l, Exception e) => CLIO l a -> (e -> CLIO l a) -> CLIO l a
catchCLIO m handler = CLIO $ do
  st <- ST.get
  (a, st') <- T.lift $ liftIO $
    DB.runRedis (clioRedisConn st) (ST.runStateT (unCLIO m) st) `catch`
    (\e -> case safeh e of
        CLIO ioe -> DB.runRedis (clioRedisConn st) $ ST.runStateT ioe st)
  ST.put st'
  return a
    where safeh e@(SomeException einner) = do
            st <- getCLIOStateTCB
            unless (clioPC st `canFlowTo` clioClr st) $ throwCLIO e
            maybe (throwCLIO e) handler $ fromException e

handle :: (Label l, Exception e) => (e -> CLIO l a) -> CLIO l a -> CLIO l a
handle = flip catchCLIO

-- | Like 'finally', but only performs the final action if there was
-- an exception raised by the computation. 
onExceptionCLIO :: Label l => CLIO l a -> CLIO l b -> CLIO l a
onExceptionCLIO io cleanup =
  io `catchCLIO` \e -> cleanup >> throwCLIO (e :: SomeException)


-- | A variant of 'bracket' where the return value from the first
-- computation is not required. 
finallyCLIO :: Label l => CLIO l a -> CLIO l b -> CLIO l a
finallyCLIO io cleanup = do
  a <- io `onExceptionCLIO` cleanup
  void cleanup
  return a

-- | When you want to acquire a resource, do some work with it, and
-- then release the resource, it is a good idea to use @bracket@,
-- because bracket will install the necessary exception handler to
-- release the resource in the event that an exception is raised
-- during the computation. If an exception is raised, then bracket
-- will re-raise the exception (after performing the release). 
bracketCLIO :: Label l => CLIO l a         -- ^ Computation to run first
            -> (a -> CLIO l c)  -- ^ Computation to run last
            -> (a -> CLIO l b)  -- ^ Computation to run in-between
            -> CLIO l b
bracketCLIO before after thing = do
  a <- before
  b <- thing a `onExceptionCLIO` after a
  void $ after a
  return b

{-| Lift an arbitrary IO action to the CLIO monad. Very unsafe! Use only in TCB.
-}
ioTCB :: IO a -> CLIO l a
ioTCB m = CLIO $ liftIO m

clioRedisGet k = CLIO $ T.lift $ DB.get k
clioRedisSet k v = CLIO $ T.lift $ DB.set k v

labelGuard :: Label l => l -> CLIO l ()
labelGuard l =
  CLIO $ do st <- ST.get
            if clioPC st `canFlowTo` l && l `canFlowTo` clioClr st
              then return ()
              else error $ "IFC error: either " ++ show (clioPC st) ++ " can't flow to " ++ show l
                     ++ " or clearance " ++ show (clioClr st) ++ " is too low for " ++ show l

taint :: Label l => l -> CLIO l ()
taint l = CLIO $ do st <- ST.get
                    let newPC = clioPC st `lub` l
                    if newPC `canFlowTo` clioClr st
                      then ST.put (st { clioPC = newPC })
                      else error $ "IFC error: clearance " ++ show (clioClr st) ++ " prevents raising PC to " ++ show newPC


class Serializable a where
  serialize :: a -> S.ByteString
  deserialize :: S.ByteString -> Either String a

data Labeled l a = LV !l !a

label :: Label l => l -> a -> CLIO l (Labeled l a)
label l x = do labelGuard l
               return (LV l x)

unlabel :: Label l => Labeled l a -> CLIO l a
unlabel (LV l x) = do taint l
                      return x

labelOf :: Label l => Labeled l a -> l
labelOf (LV l x) = l

getCLIOStateTCB :: Label l => CLIO l (CLIOState l)
getCLIOStateTCB = CLIO ST.get

putCLIOStateTCB :: Label l => CLIOState l -> CLIO l ()
putCLIOStateTCB st = CLIO (ST.put st)

getLabel :: Label l => CLIO l l
getLabel = clioPC <$> getCLIOStateTCB

getClearance :: Label l => CLIO l l
getClearance = clioClr <$> getCLIOStateTCB

lowerClearance :: Label l => l -> CLIO l ()
lowerClearance l = do labelGuard l
                      CLIO $ ST.modify (\st -> st { clioClr = l })
                      return ()

toLabeled :: Label l => l -> CLIO l a -> CLIO l (Labeled l a)
toLabeled l m =
  do st <- getCLIOStateTCB
     let (savedPC, savedClr) = (clioPC st, clioClr st)
     labelGuard l
     r <- m
     st' <- getCLIOStateTCB
     let endPC = clioPC st'
     if endPC `canFlowTo` l
       then do putCLIOStateTCB (st' { clioPC = savedPC, clioClr = savedClr }) -- NB. changes to the keymap and counter persist!
               return (LV l r)
       else error $ "toLabeled final label " ++ show endPC ++ " does not flow to supplied label " ++ show l
  
instance (Serializable l, Serializable a) => Serializable (Labeled l a) where
  serialize (LV l x) = S.pack $ show (serialize l, serialize x)
  deserialize s = case reads (S.unpack s) of
                    [((l,x),_)] -> do a <- deserialize l
                                      b <- deserialize x
                                      return (LV a b)
                    _ -> Left "Labeled deserialize error"

instance (Serializable a, Serializable b) => Serializable (a,b) where
  serialize (a,b) = S.pack $ show (serialize a, serialize b)
  deserialize s = case reads (S.unpack s) of
                    [((a,b),_)] -> do a' <- deserialize a
                                      b' <- deserialize b
                                      return (a',b')
                    _ -> Left "Pair deserialize error"

instance (Serializable a) => Serializable [a] where
  serialize as = "[" `S.append` S.intercalate "," (map serialize as) `S.append` "]"
  deserialize b =
    case S.stripPrefix "[" b of
      Just b' ->
        case S.stripSuffix "]" b' of
          Just b'' ->
            case S.split ',' b'' of
              bs -> mapM deserialize bs
          _ -> Left "List deserialize error"
      _ -> Left "List deserialize error"

instance Serializable PublicKey where
  serialize pk = B64.encode (S.pack (show pk))
  deserialize b =
    case B64.decode b of
      Right b' ->
        case reads (S.unpack b') of
          [(pk,_)] -> Right pk
          _ -> Left "PublicKey deserialize error"
      Left err -> Left err

instance Serializable PrivateKey where
  serialize sk = B64.encode (S.pack (show sk))
  deserialize b =
    case B64.decode b of
      Right b' ->
        case reads (S.unpack b') of
          [(sk,_)] -> Right sk
          _ -> Left "PrivateKey deserialize error"
      Left err -> Left err

instance Serializable S.ByteString where
  serialize b = B64.encode b
  deserialize b = B64.decode b
--
-- KeyMap
--

getKeyMap :: ST.StateT (CLIOState l) DB.Redis KeyMap
getKeyMap = clioKeyMap <$> ST.get

updateKeyMap :: Principal -> KeyPairs -> ST.StateT (CLIOState l) DB.Redis ()
updateKeyMap p kp = ST.modify (\st -> let km = clioKeyMap st
                                      in st { clioKeyMap = M.insert p kp km })
--
-- Type aliases
--

-- | A common default starting state, where @'lioLabel' = 'dcPublic'@
-- and @'lioClearance' = False '%%' True@ (i.e., the highest
-- possible clearance).
dcDefaultState :: CLIOState DCLabel
dcDefaultState = CLIOState { clioPC = dcPublic
                          , clioClr = DCLabel cFalse cTrue cTrue
                          , clioKeyMap = M.empty
                          , clioReplayCounter = M.empty
                          , clioStoreLabel = dcPublic
                          , clioRedisConn = error "no active Redis connection" }

-- | The main monad type alias to use for 'CLIO' computations that are
-- specific to 'DCLabel's.
type DC = CLIO DCLabel

-- | An alias for 'Labeled' values labeled with a 'DCLabel'.
type DCLabeled = Labeled DCLabel

-- | Wrapper function for running @'CLIO' 'DCLabel'@ computations.
--
evalDC :: DC a -> IO a
evalDC dc = evalDCWithKeyMap M.empty dc

evalDCWithKeyMap :: KeyMap -> DC a -> IO a
evalDCWithKeyMap km dc = evalCLIO (dcDefaultState { clioKeyMap = km }) dc

setPC :: DCLabel -> DC ()
setPC pc = CLIO $ ST.modify (\st -> st { clioPC = pc })

setStoreLabel :: DCLabel -> DC ()
setStoreLabel l = CLIO $ ST.modify (\st -> st { clioStoreLabel = l { dcAvailability = cTrue } })

getCount :: S.ByteString -> DC Integer
getCount v = CLIO $ do cm <- clioReplayCounter <$> ST.get
                       r <- liftIO $ getRandomBytes replayCounterSize
                       let initialCount = read (S.unpack r)
                       return (M.findWithDefault initialCount v cm)

incrementCount :: S.ByteString -> DC ()
incrementCount v = CLIO $ ST.modify (\st -> st { clioReplayCounter =
                                                 M.insertWith (const (+1)) v 1 (clioReplayCounter st) })


-- ugly but works
instance Serializable DCLabel where
  serialize = S.pack . show
  deserialize = Right . read . S.unpack

instance Serializable Integer where
  serialize = S.pack . show
  deserialize = Right . read . S.unpack
