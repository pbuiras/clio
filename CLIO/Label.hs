{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveDataTypeable #-}

module CLIO.Label (
  -- * Labels
  -- $Labels
    Label(..)
  ) where

import safe Data.Monoid ()
import safe Data.Typeable

{- $Labels

Labels are a way of describing who can observe and modify data.
Labels are governed by a partial order, generally pronounced \"can
flow to.\"  In CLIO, we write this relation ``canFlowTo``.  In the
literature, it is usually written &#8849;.

-}

-- | This class defines the operations necessary to make a label into
-- a lattice (see <http://en.wikipedia.org/wiki/Lattice_(order)>).
-- 'canFlowTo' partially orders labels.
-- 'lub' and 'glb' compute the least upper bound and greatest lower
-- bound of two labels, respectively.
class (Eq l, Show l, Read l, Typeable l) => Label l where
  -- | Compute the /least upper bound/, or join, of two labels.  When
  -- data carrying two different labels is mixed together in a
  -- document, the @lub@ of the two labels is the lowest safe value
  -- with which to label the result.
  --
  -- More formally, for any two labels @l1@ and @l2@, if @ljoin = l1
  -- \`lub` l2@, it must be that:
  --
  -- * @L_1 ``canFlowTo`` ljoin@,
  --
  -- * @L_2 ``canFlowTo`` ljoin@, and
  --
  -- * There is no label @l /= ljoin@ such that @l1 ``canFlowTo`` l@,
  --   @l2 ``canFlowTo`` l@, and @l ``canFlowTo`` ljoin@.  In other
  --   words @ljoin@ is the least element to which both @l1@ and @l2@
  --   can flow.
  --
  -- When used infix, has fixity:
  --
  -- > infixl 5 `lub`
  lub :: l -> l -> l

  -- | /Greatest lower bound/, or meet, of two labels. For any two
  -- labels @l1@ and @l2@, if @lmeet = l1 \`glb` l2@, it must be
  -- that:
  --
  -- * @lmeet ``canFlowTo`` l1@,
  --
  -- * @lmeet ``canFlowTo`` l2@, and
  --
  -- * There is no label @l /= lmeet@ such that @l ``canFlowTo`` l1@,
  --   @l ``canFlowTo`` l2@, and @lmeet ``canFlowTo`` l@.  In other
  --   words @lmeet@ is the greatest element flowing to both @l1@ and
  --   @l2@.
  --
  -- When used infix, has fixity:
  --
  -- > infixl 5 `glb`
  glb :: l -> l -> l

  -- | /Can-flow-to/ relation (&#8849;). An entity labeled @l1@ should
  -- be allowed to affect an entity @l2@ only if @l1 \`canFlowTo`
  -- l2@. This relation on labels is at least a partial order (see
  -- <https://en.wikipedia.org/wiki/Partially_ordered_set>), and must
  -- satisfy the following laws:
  --
  -- * Reflexivity: @l1 \`canFlowTo` l1@ for any @l1@.
  --
  -- * Antisymmetry: If @l1 \`canFlowTo` l2@ and
  --   @l2 \`canFlowTo` l1@ then @l1 = l2@.
  --
  -- * Transitivity: If @l1 \`canFlowTo` l2@ and
  --   @l2 \`canFlowTo` l3@ then @l1 \`canFlowTo` l3@.
  --
  -- When used infix, has fixity:
  --
  -- > infix 4 `canFlowTo`
  canFlowTo :: l -> l -> Bool

infixl 5 `lub`, `glb`
infix 4 `canFlowTo`
