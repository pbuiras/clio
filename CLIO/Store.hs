{-# LANGUAGE ScopedTypeVariables #-}
module CLIO.Store where

import CLIO.Core
import CLIO.DC
import CLIO.Crypto
import CLIO.Label

import Data.Maybe
import Control.Monad.Trans
import Control.Monad
import Control.Exception

import Crypto.PubKey.RSA

import Crypto.Hash.Algorithms

import qualified Data.ByteString.Char8 as B
import qualified Data.Map as M
import qualified Data.Set as S

import qualified Control.Monad.State.Strict as ST

store :: Serializable a => B.ByteString -> DCLabeled a -> DC ()
store k (LV l v) =
  (do -- begin IFC checks
      storeGuard
      labelGuard l
      -- end IFC checks
      pks <- fetchSymKeysFor l
      sks <- fetchSignatureKeysFor l
      incrementCount k
      n <- getCount k
      let x = serialize (k,(v,n))
      sigs <- clioMultiSign sks x
      b <- clioEncrypt pks (serialize (x:sigs))
      clioRedisSet k $ serialize (LV l b)
      return ()) `catchCLIO`
  (\(e::SomeException) ->
     do ioTCB $ putStrLn $ "Exception when storing at key " ++ show k
        throwCLIO e)

                           
fetch :: Serializable a => B.ByteString -> DCLabeled a -> DC (DCLabeled a)
fetch k lv@(LV l1 a) =
  fetchGuard l1 >>
 (do    Right (Just lct) <- clioRedisGet k
        let Right (LV l b) = deserialize lct
        if l `canFlowTo` l1
          then do sks <- fetchSymKeysFor l
                  pks <- fetchVerificationKeysFor l
                  val <- clioDecrypt sks b
                  let Right (dval:sigs) = deserialize val
                  if clioMultiVerify pks dval sigs
                    then do let Right (k',(v,n)) = deserialize dval
                            c <- getCount k
                            if n < c || k /= k'
                              then return lv
                              else return (LV l v)
                    else do ioTCB (putStrLn "verification failed"); return lv
          else return lv) `catchCLIO`
  (\(e::SomeException) ->
     do ioTCB $ putStrLn $ "Exception when fetching from key " ++ show k ++ ": " ++ show e
        return lv)

storeGuard :: DC ()
storeGuard = do
  storeLabel <- clioStoreLabel <$> CLIO ST.get
  labelGuard storeLabel

fetchGuard :: DCLabel -> DC ()
fetchGuard l = do
  storeLabel <- clioStoreLabel <$> CLIO ST.get
  if dcAvailability storeLabel `cImplies` dcAvailability l
    then return ()
    else error $ "Availability guard failed in fetch: " ++ show (dcAvailability storeLabel) ++ " can't flow to " ++ show (dcAvailability l)
