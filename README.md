# CLIO #

## Instructions ##

There should be a redis server running locally on the default port.

**Installing redis:** `brew install redis` worked for me on OSX. The install scripts give you instructions on how to start the server.

** Compiling and running the code: ** Try running

```
#!bash

$ cabal install --dependencies-only
$ cabal repl
```

And then


```
#!haskell

> :l Main
> main
```

## Case Study ##

You can find a case study in the [clio-taxprep
repository](https://bitbucket.org/pbuiras/clio-taxprep) (check README
for installation instructions).